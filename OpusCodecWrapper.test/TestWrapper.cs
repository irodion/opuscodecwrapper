﻿using System;
using NUnit.Framework;
using OpusCodecWrapper;
using OpusCodecWrapper.DataTypes;

namespace OpusCodecWrapper.test
{
    [TestFixture]
    public class TestWrapper
    {
        [TestCase]
        public void TestGetVersion()
        {
            OpusLibraryInfo libraryInfo = new OpusLibraryInfo();
            string versionInfo = libraryInfo.Version;
            Assert.AreNotEqual(string.Empty, versionInfo);
        }

        [TestCase]
        public void TestCreateAndCloseEncoder()
        {
            OpusEncoder encoder = new OpusEncoder(OpusSamplingRate.Sampling48000, OpusChannels.Stereo, OpusApplicationType.Audio);
            Assert.AreNotEqual(null, encoder);
        }

        [TestCase]
        [Ignore("Not implemented yet")]
        public void TestEncodeNullData()
        {
            
        }

        [TestCase]
        [Ignore("Not implemented yet")]
        public void TestCreateAndCloseDecoder()
        {
           
        }

    }
}
