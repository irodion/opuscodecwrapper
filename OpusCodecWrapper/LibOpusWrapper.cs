﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using OpusCodecWrapper.DataTypes;

namespace OpusCodecWrapper
{
    internal static class LibOpusWrapper
    {


        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr opus_get_version_string();
        
        // encoder specific

        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr opus_encoder_create([In] OpusSamplingRate sampling, [In] OpusChannels channels,
            [In] OpusApplicationType applicationType, [Out] out OpusStatusCodes errorCode);

        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern OpusStatusCodes opus_encoder_init(IntPtr st, [In] OpusSamplingRate fs,
            [In] OpusChannels channels, [In] OpusApplicationType application);

        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int opus_encoder_get_size([In] OpusChannels channels);

        [SuppressUnmanagedCodeSecurity()]
        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int opus_encode([In] IntPtr st, [In] short[] pcm, [In] Int32 frameSize,
             [Out] byte[] data, [In] Int32 maxDataBytes);

        [SuppressUnmanagedCodeSecurity()]
        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int opus_encode_float([In]IntPtr st, [In] float[] pcm, [In] Int32 frameSize,
             [Out]byte[] data, [In]Int32 maxDataBytes);

        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int opus_encoder_ctl([In] IntPtr st, [In]OpusCtlSetRequest request,
            [In]int value);

        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int opus_encoder_ctl([In] IntPtr st, [In] OpusCtlGetRequest request,
            [Out] out int value);

        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void opus_encoder_destroy([In, Out]IntPtr st);

        // decoder specifics
        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern IntPtr opus_decoder_create([In]OpusSamplingRate samplingRate, 
            [In]OpusChannels channels, [Out] out OpusStatusCodes codes );

        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern void opus_decoder_destroy(IntPtr decoder);

        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int opus_decode([In] IntPtr decoder, [In] byte[] data, [In] Int32 len,
            [Out] out byte[] pcm,
            [In] Int32 frameSize, [In] Int32 decodeFec);

        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int opus_decode_float([In] IntPtr decoder, [In] byte[] data, [In] Int32 len,
            [Out] out float[] pcm,
            [In] Int32 frameSize, [In] Int32 decodeFec);

        [DllImport("opus.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        public static extern int opus_decoder_get_size([In] OpusChannels channels);

    }
}
