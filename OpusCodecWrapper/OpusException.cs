﻿
using System;
using OpusCodecWrapper.DataTypes;

namespace OpusCodecWrapper
{
    public class OpusException : Exception
    {
        public OpusStatusCodes OpusStatusCodes { get; private set; }

        /// <summary>
        /// Create an extended exception for OpusCodecWrapper
        /// </summary>
        /// <param name="codes">Error code</param>
        /// <param name="message">Message</param>
        public OpusException(OpusStatusCodes codes, String message) : base(message)
        {
            OpusStatusCodes = codes;
        }


    }
}
