﻿

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace OpusCodecWrapper
{
    public class OpusLibraryInfo
    {
        /// <summary>
        /// Library version information
        /// </summary>
        public string Version { get; private set; }
        /// <summary>
        /// libopus dll not found
        /// </summary>
        public bool DllNotFound { get; private set; }

        private void GetVersionInfo()
        {
            IntPtr ptrVersion = LibOpusWrapper.opus_get_version_string();
            if (IntPtr.Zero != ptrVersion)
            {
                Version = Marshal.PtrToStringAnsi(ptrVersion);    
            }
        }


        public OpusLibraryInfo()
        {
            try
            {
                GetVersionInfo();
            }
            catch (System.DllNotFoundException)
            {
                DllNotFound = true;
            }
            
        }
    }
}
