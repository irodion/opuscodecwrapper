﻿using System;
using System.Data;
using System.Diagnostics.Contracts;
using System.Runtime.InteropServices;
using OpusCodecWrapper.DataTypes;


namespace OpusCodecWrapper
{
    public class OpusEncoder : IDisposable
    {

        private IntPtr _encoderPtr = IntPtr.Zero;
        private OpusSamplingRate _samplingRate = OpusSamplingRate.Sampling48000;
        private OpusChannels _channels = OpusChannels.Stereo;
        private int maxDataBytes = 4000;

        public int FrameSizePerChannel { get; private set; }


        /// <summary>
        /// Opus encoder
        /// </summary>
        /// <param name="samplingRate">Sampling rate of input signal (Hz) This must be one of 8000, 12000, 16000, 24000, or 48000</param>
        /// <param name="channels">Number of channels (1 or 2) in input signal</param>
        /// <param name="bitrate">Bitrate</param>
        /// <param name="applicationType">Coding mode</param>
        /// <param name="delays">Delays</param>
        public OpusEncoder(OpusSamplingRate samplingRate = OpusSamplingRate.Sampling48000,
            OpusChannels channels = OpusChannels.Stereo,
            OpusApplicationType applicationType = OpusApplicationType.Audio)
        {

            //check all parameters 
            if (!Enum.IsDefined(typeof (OpusSamplingRate), samplingRate))
            {
                throw new ArgumentOutOfRangeException(@"Sampling rate out of range");
            }

            if (!Enum.IsDefined(typeof (OpusChannels), channels))
            {
                throw new ArgumentOutOfRangeException(@"Channels out of range");
            }

            if (!Enum.IsDefined(typeof (OpusApplicationType), applicationType))
            {
                throw new ArgumentOutOfRangeException(@"Application out of range");
            }

            _samplingRate = samplingRate;
            _channels = channels;

            OpusStatusCodes statusCodes;
            _encoderPtr = LibOpusWrapper.opus_encoder_create(_samplingRate, _channels, applicationType, out statusCodes);
        }

        /// <summary>
        /// Encodes Opus frame 
        /// </summary>
        /// <param name="pcm">Input signal (interleaved if 2 channels)</param>
        /// <returns></returns>
        public byte[] encodeOpus(short[] pcm)
        {
            Contract.Requires<ArgumentNullException>(null != pcm);
            
            byte[] data = new byte[maxDataBytes];
            int result = LibOpusWrapper.opus_encode(_encoderPtr, pcm, FrameSizePerChannel, data, maxDataBytes);

            return (data);
            Contract.Ensures(null != data);
            Contract.Ensures(result >= 0);
        }

        /// <summary>
        /// Close encoder
        /// </summary>
        private void Close()
        {
            if (IntPtr.Zero != _encoderPtr)
            {
                LibOpusWrapper.opus_encoder_destroy(_encoderPtr);
                _encoderPtr = IntPtr.Zero;
            }
        }

        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(IntPtr.Zero != _encoderPtr);
            Contract.Invariant(0 != FrameSizePerChannel);
            Contract.Invariant(0 != maxDataBytes);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~OpusEncoder()
        {
            Dispose(false);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                //free managed resources if any        
            }
            Close();
        }


    }
}
