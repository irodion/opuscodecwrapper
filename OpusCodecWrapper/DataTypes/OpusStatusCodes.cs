﻿

namespace OpusCodecWrapper.DataTypes
{
    /// <summary>
    /// Error codes
    /// </summary>
    public enum OpusStatusCodes : int 
    {
        /// <summary>
        /// No error.
        /// </summary>
        Ok = 0,
        /// <summary>
        /// One or more invalid/out of range arguments.
        /// </summary>
        BadArguments = -1,
        /// <summary>
        /// The mode struct passed is invalid.
        /// </summary>
        BufferTooSmall = -2,
        /// <summary>
        /// An internal error was detected.
        /// </summary>
        InternalError = -3,
        /// <summary>
        /// The compressed data passed is corrupted.
        /// </summary>
        InvalidPacket = -4,
        /// <summary>
        /// Invalid/unsupported request number.
        /// </summary>
        Unimplemented = -5,
        /// <summary>
        /// An encoder or decoder structure is invalid or already freed.
        /// </summary>
        InvalidState = -6,
        /// <summary>
        /// Memory allocation has failed.
        /// </summary>
        AllocFail = -7
    }
}
