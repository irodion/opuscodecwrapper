﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpusCodecWrapper.DataTypes
{
    public enum OpusApplicationType : int
    {
        /// <summary>
        /// Best for VOIP aplications
        /// </summary>
        Voip = 2048,
        /// <summary>
        /// Use for music and mixed content
        /// </summary>
        Audio = 2049,
        /// <summary>
        /// Disable speech optimized mode
        /// </summary>
        RestrictedLowDelay = 2051
    }
}
