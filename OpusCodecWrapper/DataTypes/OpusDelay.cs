﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpusCodecWrapper.DataTypes
{
    public enum OpusDelays : int
    {
        /// <summary>
        /// 2.5ms
        /// </summary>
        Delay2Dot5ms = 5,
        /// <summary>
        /// 5ms
        /// </summary>
        Delay5ms = 10,
        /// <summary>
        /// 10ms
        /// </summary>
        Delay10ms = 20,
        /// <summary>
        /// 20ms
        /// </summary>
        Delay20ms = 40,
        /// <summary>
        /// 40ms
        /// </summary>
        Delay40ms = 80,
        /// <summary>
        /// 60ms
        /// </summary>
        Delay60ms = 120
    }
}
