﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpusCodecWrapper.DataTypes
{
    public enum OpusSamplingRate: int
    {
        Sampling08000 = 8000,
        Sampling12000 = 12000,
        Sampling16000 = 16000,
        Sampling24000 = 24000,
        Sampling48000 = 48000
    }
}
