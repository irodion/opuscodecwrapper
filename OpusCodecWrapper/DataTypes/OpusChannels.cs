﻿
namespace OpusCodecWrapper.DataTypes
{
    /// <summary>
    /// Number of channels (1 or 2)
    /// </summary>
    public enum OpusChannels : int
    {
        /// <summary>
        /// 1 Channel
        /// </summary>
        Mono = 1,

        /// <summary>
        /// 2 Channels
        /// </summary>
        Stereo = 2
    }
}
