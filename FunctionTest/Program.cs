﻿using System;
using OpusCodecWrapper;
using OpusCodecWrapper.DataTypes;

namespace FunctionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            GetLibraryInfo();
            //OpusEncoder encoder = OpenEncoder();

            //try to encode data

            Console.WriteLine("Press any key...");
            Console.ReadLine();
        }

        private static void GetLibraryInfo()
        {
            OpusLibraryInfo libraryInfo = new OpusLibraryInfo();

            Console.WriteLine(@"Opus wrapper library console test");
            string opusVersion = libraryInfo.Version;
            if (libraryInfo.DllNotFound)
            {
                Console.WriteLine(@"libopus.dll not found");
                return;
            }
            else
            {
                Console.WriteLine(@"libopus.dll found, get version..");
            }

            Console.WriteLine(@"Library version: " + libraryInfo.Version);
        }

        private static OpusEncoder OpenEncoder()
        {
            Console.WriteLine(@"Opus open encoder library ");
            OpusEncoder encoder = new OpusEncoder(OpusSamplingRate.Sampling48000, OpusChannels.Stereo, OpusApplicationType.Audio);
            return (encoder);
        }

    }
}
